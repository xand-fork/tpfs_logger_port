#[macro_export]
macro_rules! trace {
    ( $e:expr ) => {
        $crate::__inner_log!($e, $crate::log::Level::Trace)
    };
}

#[macro_export]
macro_rules! debug {
    ( $e:expr ) => {
        $crate::__inner_log!($e, $crate::log::Level::Debug)
    };
}

#[macro_export]
macro_rules! info {
    ( $e:expr ) => {
        $crate::__inner_log!($e, $crate::log::Level::Info)
    };
}

#[macro_export]
macro_rules! warn {
    ( $e:expr ) => {
        $crate::__inner_log!($e, $crate::log::Level::Warn)
    };
}

#[macro_export]
macro_rules! error {
    ( $e:expr ) => {
        $crate::__inner_log!($e, $crate::log::Level::Error)
    };
}

#[doc(hidden)]
#[macro_export]
macro_rules! __inner_log {
    ( $e:expr, $sev:expr ) => {{
        $crate::log_event($e, $sev, module_path!(), module_path!(), file!(), line!())
    }};
}
